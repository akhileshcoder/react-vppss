import React from 'react';
import { render } from 'react-dom';
import { Router, Route, hashHistory } from 'react-router';
import App from './components/App';
import Vivek from './components/Vivek';
import Prashanthi from './components/Prashanthi';
import Priyanka from './components/Priyanka';
import Srikanth from './components/Srikanth';
import Suma from './components/Suma';

window.React = React;

render(
  (<Router history={hashHistory}>
    <Route path="/" component={App}>
      <Route path="/vivek" component={Vivek} />
      <Route path="/suma" component={Suma} />
      <Route path="/prashanthi" component={Prashanthi} />
      <Route path="/srikanth" component={Srikanth} />
      <Route path="/priyanka" component={Priyanka} />
    </Route>
  </Router>), document.getElementById('content')
);
